﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace XamarinApp.Models
{
    public class MasterMenuItem
    {
        public string Title { get; set; }
        public string IconSource { get; set; }
        public Color BackGroundColor { get; set; }
        public Type TargetType { get; set; }
        
        public MasterMenuItem(string title, string iconsource, Color color, Type type)
        {
            this.Title = title;
            this.IconSource = iconsource;
            this.BackGroundColor = color;
            this.TargetType = type;
        }

    }
}
