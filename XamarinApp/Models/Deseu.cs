﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinApp.Models
{
    [Serializable]
    public class Deseu

    {
        string Type { get; set; }
        string Name { get; set; }
        int dangerLevel { get; set; }

        public Deseu(string Type, string Name, int dangerLevel)
        {
            this.Type = Type;
            this.Name = Name;
            this.dangerLevel = dangerLevel;
        }



    }
}
