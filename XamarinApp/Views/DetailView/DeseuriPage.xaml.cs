﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.Models;
namespace XamarinApp.Views.DetailView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeseuriPage : ContentPage
    {
        public DeseuriPage()
        {
            InitializeComponent();
        }

        async void saveBtn_Clicked(object sender, EventArgs e)
        {
            Deseu deseu = new Deseu(deseuriPicker.SelectedItem.ToString(), text.Text, int.Parse(number.Text.ToString()));
            var opt = new JsonSerializerOptions() { WriteIndented=true};
            string output = System.Text.Json.JsonSerializer.Serialize<Deseu>(deseu, opt);
            await DisplayAlert("You have reported a violation!", output, "Ok");
            //await DisplayAlert("You have reported a violation!",
            //    " You reported: " + deseuriPicker.SelectedItem.ToString() +
            //    " Your name is: " + text.Text + " Danger level: " + number.Text.ToString(), "OK");
        }
        
    }
}