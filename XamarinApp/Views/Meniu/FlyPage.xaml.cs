﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.Models;
using FlyoutItem = XamarinApp.Models.FlyoutItem;

namespace XamarinApp.Views.Meniu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FlyPage : FlyoutPage
    {
        public FlyPage()
        {
            InitializeComponent();
            flyout.listview.ItemSelected += OnSelectedItem;
        }

        private void OnSelectedItem(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as FlyoutItem;
            if(item != null)
            {

                Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetPage));
                flyout.listview.SelectedItem = null;
                IsPresented = false;
            }
        }
    }
}